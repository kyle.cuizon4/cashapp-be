const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const { OAuth2Client } = require('google-auth-library')
const clientId = '769488498495-bsp03vce5kp7ccdb1qpsatnoj7cl2ica.apps.googleusercontent.com'


//function to catch & handle errors
const errCatcher = err => console.log(err)

//function for finding duplicate emails
module.exports.emailExists = (params) => {
    //find a user document with matching email
    return User.find({ email: params.email })
    .then(result => {
        //if match found, return true
		return result.length > 0 ? true : false
    })
    .catch(errCatcher)
}

module.exports.register = (params) => {
    //instantiate a new user object
	let user = new User({
        firstName : params.firstName,
        lastName : params.lastName,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10)
	})

    //save user object as a new document
    return user.save()
    .then((user, err) => {
        //if err generated, return false otherwise return true
		return (err) ? false : true
    })
    .catch(errCatcher)
}


module.exports.login = (params) => {
    return User.findOne({ email: params.email }).then(user => {
        if (user === null) { return false }

        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

        if (isPasswordMatched) {
            return { accessToken: auth.createAccessToken(user.toObject()) }
        } else {
            return false
        }
    })
}

//function for getting details of a user based on a decoded token
module.exports.getPrivate = (params) => {
    return User.findById(params.userId)
    .then(user => {
        //clear the password property of the user object for security
		user.password = undefined
		return user
    })
    .catch(errCatcher)
}



module.exports.transact = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.transactions.push(params.transaction)
        return user.save()
        .then((updatedUser, err) => {
            return err ? false : true
        })
        .catch(errCatcher)
    })
    .catch(errCatcher)
}




module.exports.verifyGoogleTokenId = async (tokenId) => {
    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({ idToken: tokenId, audience: clientId })

    if (data.payload.email_verified === true) {
        const user = await User.findOne({ email: data.payload.email }).exec()

        if (user !== null) {
            if (user.loginType === 'google') {
                return { accessToken: auth.createAccessToken(user.toObject()) }
            } else {
                return { error: 'login-type-error' }
            }
        } else {
            
            const newUser = new User({

                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                password : data.payload.password,
                email : data.payload.email,
                loginType: 'google'
            })

            return newUser.save().then((user, err) => {
                console.log(user)
                return { accessToken: auth.createAccessToken(user.toObject()) }
            })
        }
    } else {
        return { error: 'google-auth-error' }
    }
}