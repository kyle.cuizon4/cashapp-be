
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 4000; 
const cors = require('cors');

app.use(cors());

//connection to database
mongoose.connection.once('open',() => console.log('Now connected to the data base'));
// const database = 'mongodb://localhost:27017/budget_tracker';
const database = 'mongodb+srv://kylePC:kyle@batch73.c1d9n.mongodb.net/budget_app?retryWrites=true&w=majority'      

mongoose.connect(database, {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

//middleware
app.use(express.json())


// routes
const userRoutes = require('./routes/user');
app.use('/api/users', userRoutes);


app.listen( port , () => {
	console.log(`API is now online on port ${port}`);
})