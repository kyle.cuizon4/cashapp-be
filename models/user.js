const mongoose = require('mongoose')
const moment = require('moment')

const userSchema = new mongoose.Schema({

    firstName: {
        type  : String,
        required : [true, 'First Name required']
    },

    lastName : {
        type : String,
        required : [true, "Last Name required"]
    },

    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    loginType : {
        type: String,
        // required :[true , 'Login Type is required.']
    },
    password: {
        type: String,
        // required: [true, 'Password is required']
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    transactions: [
        {
            date: {
                type: Date,
                default : Date.now
            },
            category : {
                type: String,
                default: "Not Specified"
            },
            type: {
                type : String,
                required: [true, 'Type is required.']
            },
            amount: {
                type: Number,
                required: [true, 'Distance is required.']
            },
            name: {
                type: String,
                required: [true, 'Description name is required.']
            }

        }
    ]
})

module.exports = mongoose.model('user', userSchema)