const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})


router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

//for getting private user profile info
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.getPrivate({ userId: user.id }).then(user => res.send(user))
})

//for recording user transactions
router.post('/transactions', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        transaction: {
            type : req.body.type,
            amount : req.body.amount,
            name : req.body.name,
            category : req.body.category
        }
    }
    UserController.transact(params).then(result => res.send(result))
})

router.post('/verify-google-id-token', async (req, res) => {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})


module.exports = router

