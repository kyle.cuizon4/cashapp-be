# SETUP INSTRUCTIONS
1. Give this codebase to the students as boilerplate.

2. Have them install all dependencies via the terminal command:
> npm install

3. Have the students create a .env file at the root directory of the project.

4. In this .env file, have them set the following local environment variables:
- PORT (port number to be used when API is served)
- DB_MONGODB (mongoDB connection string)
- JWT_SECRET (any string to be used in signing a JWT)

5. Have the students serve the API via the terminal command:
> nodemon index.js